package com.kodality.lycido;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import com.nimbusds.oauth2.sdk.AccessTokenResponse;
import com.nimbusds.oauth2.sdk.AuthorizationGrant;
import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Response;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.TokenErrorResponse;
import com.nimbusds.oauth2.sdk.TokenRequest;
import com.nimbusds.oauth2.sdk.auth.ClientAuthentication;
import com.nimbusds.oauth2.sdk.auth.PlainClientSecret;
import com.nimbusds.oauth2.sdk.auth.verifier.ClientAuthenticationVerifier;
import com.nimbusds.oauth2.sdk.auth.verifier.ClientCredentialsSelector;
import com.nimbusds.oauth2.sdk.client.ClientInformation;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.token.Tokens;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
public class TokenRoute extends NimbusRoute {

  private TokenStore tokenStore;
  private UserinfoProvider userinfoProvider;
  private ClientCredentialsSelector<Object> clientCredentialsSelector;
  private ClientStore clientStore;

  @Override
  public Response handle(HTTPRequest req) throws Exception {
    try {
      TokenRequest tokenRequest = TokenRequest.parse(req);
      AuthorizationGrant authorizationGrant = tokenRequest.getAuthorizationGrant();
      if (authorizationGrant.getType() == GrantType.CLIENT_CREDENTIALS) {
        ClientAuthentication clientAuthentication = tokenRequest.getClientAuthentication();
        if (clientAuthentication instanceof PlainClientSecret) {
          ClientAuthenticationVerifier<Object> clientAuthenticationVerifier =
              new ClientAuthenticationVerifier<>(clientCredentialsSelector,
                                                 Collections.singleton(new Audience("not_used")));
          clientAuthenticationVerifier.verify(clientAuthentication, null, null);
          ClientInformation clientInformation = clientStore.get(clientAuthentication.getClientID().getValue());
          Scope scope = new Scope();
          if (clientInformation.getMetadata().getScope() != null) {
            scope.addAll(clientInformation.getMetadata().getScope());
          }
          if (tokenRequest.getScope() != null) {
            scope.retainAll(tokenRequest.getScope());
          }
          BearerAccessToken accessToken = new BearerAccessToken(3600, scope);
          tokenStore.put(accessToken, userinfoProvider.get(clientAuthentication.getClientID()));
          Tokens tokens = new Tokens(accessToken, null);
          return new AccessTokenResponse(tokens);
        } else {
          throw new GeneralException(OAuth2Error.ACCESS_DENIED);
        }
      } else {
        throw new GeneralException(OAuth2Error.UNSUPPORTED_GRANT_TYPE);
      }
    } catch (GeneralException e) {
      if (e.getErrorObject() != null) {
        return new TokenErrorResponse(e.getErrorObject());
      } else {
        throw e;
      }
    }
  }
  
  
  @Override
  @PostMapping("/token")
  public Object handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
    return super.handle(request, response);
  }
}
