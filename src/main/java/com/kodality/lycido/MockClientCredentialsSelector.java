package com.kodality.lycido;

import java.security.PublicKey;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.nimbusds.jose.JWSHeader;
import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.auth.verifier.ClientCredentialsSelector;
import com.nimbusds.oauth2.sdk.auth.verifier.Context;
import com.nimbusds.oauth2.sdk.auth.verifier.InvalidClientException;
import com.nimbusds.oauth2.sdk.id.ClientID;

public class MockClientCredentialsSelector<T> implements ClientCredentialsSelector<T> {

  @Override
  public List<Secret> selectClientSecrets(ClientID claimedClientID,
                                          ClientAuthenticationMethod authMethod,
                                          Context<T> context)
      throws InvalidClientException {
    return Arrays.asList(new Secret("yupi" + claimedClientID.getValue()));
  }

  @Override
  public List<? extends PublicKey> selectPublicKeys(ClientID claimedClientID,
                                                    ClientAuthenticationMethod authMethod,
                                                    JWSHeader jwsHeader,
                                                    boolean forceRefresh,
                                                    Context<T> context)
      throws InvalidClientException {
    return Collections.emptyList();
  }

}
