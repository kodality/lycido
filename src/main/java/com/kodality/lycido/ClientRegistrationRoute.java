package com.kodality.lycido;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.Response;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.client.ClientInformation;
import com.nimbusds.oauth2.sdk.client.ClientInformationResponse;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.client.ClientRegistrationErrorResponse;
import com.nimbusds.oauth2.sdk.client.ClientRegistrationRequest;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.id.ClientID;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
public class ClientRegistrationRoute extends NimbusRoute {
  private ClientStore clientStore;

  @Override
  public Response handle(HTTPRequest req) throws Exception {
    try {
      ClientRegistrationRequest registrationRequest = ClientRegistrationRequest.parse(req);

      ClientMetadata metadata = registrationRequest.getClientMetadata();
      String id = generateClientId();
      ClientInformation clientInfo = new ClientInformation(new ClientID(id), new Date(), metadata, new Secret(generateSecret()));
      clientStore.put(id, clientInfo);
      
      return new ClientInformationResponse(clientInfo);
    } catch (GeneralException e) {
      if (e.getErrorObject() != null) {
        return new ClientRegistrationErrorResponse(e.getErrorObject());
      } else {
        throw e;
      }
    }
  }

  private String generateClientId() {
    return "generatedClient-" + RandomStringUtils.randomAlphanumeric(60);
  }
  
  private String generateSecret() {
    return "generatedSecret-" + RandomStringUtils.randomAlphanumeric(120);
  }
  
  @Override
  @PostMapping("/register")
  public Object handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
    return super.handle(request, response);
  }
}
