package com.kodality.lycido;

import com.nimbusds.oauth2.sdk.client.ClientInformation;

public interface ClientStore extends Store<String, ClientInformation> {

}
