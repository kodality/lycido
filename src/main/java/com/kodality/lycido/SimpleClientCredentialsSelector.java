package com.kodality.lycido;

import java.security.PublicKey;
import java.util.Arrays;
import java.util.List;

import com.nimbusds.jose.JWSHeader;
import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.auth.verifier.ClientCredentialsSelector;
import com.nimbusds.oauth2.sdk.auth.verifier.Context;
import com.nimbusds.oauth2.sdk.auth.verifier.InvalidClientException;
import com.nimbusds.oauth2.sdk.client.ClientInformation;
import com.nimbusds.oauth2.sdk.id.ClientID;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SimpleClientCredentialsSelector implements ClientCredentialsSelector<Object> {

  private ClientStore clientStore;

  @Override
  public List<Secret> selectClientSecrets(ClientID claimedClientID,
                                          ClientAuthenticationMethod authMethod,
                                          Context<Object> context)
      throws InvalidClientException {

    ClientInformation clientInformation = clientStore.get(claimedClientID.getValue());
    if (clientInformation != null) {
      return Arrays.asList(clientInformation.getSecret());
    }
    return null;
  }

  @Override
  public List<? extends PublicKey> selectPublicKeys(ClientID claimedClientID,
                                                    ClientAuthenticationMethod authMethod,
                                                    JWSHeader jwsHeader,
                                                    boolean forceRefresh,
                                                    Context<Object> context)
      throws InvalidClientException {
    return null;
  }

}
