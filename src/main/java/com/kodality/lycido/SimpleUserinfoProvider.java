package com.kodality.lycido;

import com.nimbusds.oauth2.sdk.client.ClientInformation;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;

import lombok.AllArgsConstructor;
import net.minidev.json.JSONObject;

@AllArgsConstructor
public class SimpleUserinfoProvider implements UserinfoProvider {
  private ClientStore clientStore;
  
  @Override
  public UserInfo get(ClientID id) {
    ClientInformation clientInformation = clientStore.get(id.getValue());
    if (clientInformation != null) {
      JSONObject field = (JSONObject) clientInformation.getMetadata().getCustomField("userinfo");
      field.put("sub", id.getValue());
      return new UserInfo(field);
    }
    return null;
  }

}
