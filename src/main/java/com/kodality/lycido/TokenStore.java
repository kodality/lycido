package com.kodality.lycido;

import org.apache.commons.lang3.tuple.Pair;

import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;

public interface TokenStore {

  void put(AccessToken at, UserInfo userInfo);

  Pair<AccessToken, UserInfo> get(String value);
}
