package com.kodality.lycido.exp;

import org.apache.commons.lang3.tuple.Pair;

import com.kodality.lycido.TokenStore;
import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.Response;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerTokenError;
import com.nimbusds.openid.connect.sdk.UserInfoErrorResponse;
import com.nimbusds.openid.connect.sdk.UserInfoRequest;
import com.nimbusds.openid.connect.sdk.UserInfoSuccessResponse;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UserinfoRoute2 extends NimRoute<UserInfoRequest> {

  private TokenStore tokenStore;
  
  {
    parser = UserInfoRequest::parse;
    errorHandler = UserInfoErrorResponse::new;
  }

  @Override
  public Response handle(UserInfoRequest req) throws Exception {
    AccessToken accessToken = req.getAccessToken();
    Pair<AccessToken, UserInfo> pair = tokenStore.get(accessToken.getValue());
    if (pair != null && pair.getRight() != null) {
      pair.getRight().setClaim("scope", pair.getLeft().getScope());
      return new UserInfoSuccessResponse(pair.getRight());
    } else {
      throw new GeneralException(BearerTokenError.INVALID_TOKEN);
    }
  }

}
