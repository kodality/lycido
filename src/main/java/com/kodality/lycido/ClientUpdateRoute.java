package com.kodality.lycido;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;

import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.Response;
import com.nimbusds.oauth2.sdk.auth.Secret;
import com.nimbusds.oauth2.sdk.client.ClientInformation;
import com.nimbusds.oauth2.sdk.client.ClientInformationResponse;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.client.ClientRegistrationErrorResponse;
import com.nimbusds.oauth2.sdk.client.ClientUpdateRequest;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.id.ClientID;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
public class ClientUpdateRoute extends NimbusRoute {

  @Override
  public Response handle(HTTPRequest req) throws Exception {
    try {
      ClientUpdateRequest updateRequest = ClientUpdateRequest.parse(req);

      ClientMetadata metadata = updateRequest.getClientMetadata();
      System.out.println(metadata);

      return new ClientInformationResponse(new ClientInformation(new ClientID("bla"), new Date(), metadata, new Secret("bla")));
    } catch (GeneralException e) {
      if (e.getErrorObject() != null) {
        return new ClientRegistrationErrorResponse(e.getErrorObject());
      } else {
        throw e;
      }
    }
  }
  
  @Override
  @PutMapping("/register")
  public Object handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
    return super.handle(request, response);
  }
}
