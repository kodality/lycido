package com.kodality.lycido;

import org.springframework.jdbc.core.JdbcTemplate;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.client.ClientInformation;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;

@AllArgsConstructor
@Slf4j
public class SqlClientStore implements ClientStore {

  private JdbcTemplate jdbcTemplate;

  @Override
  public ClientInformation get(String k) {
    try {
      String json = jdbcTemplate.queryForObject("SELECT info FROM clients WHERE id = ?", String.class, k);
      JSONObject jsonObject = JSONObjectUtils.parse(json);
      return ClientInformation.parse(jsonObject);
    } catch (ParseException e) {
      log.warn("Could not parse client information from the DB by key: %s ", k);
    }
    return null;
  }

  @Override
  public void put(String k, ClientInformation v) {
    jdbcTemplate.update("INSERT INTO clients (id, info) VALUES (?, ?)", k, v.toJSONObject().toJSONString());
  }

  @Override
  public void remove(String k) {
    jdbcTemplate.update("DELETE FROM clients WHERE id = ?", k);
  }

}
